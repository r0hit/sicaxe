
package arch.sic;

import java.util.ArrayList;

import emu.sic.DataLogger;
import asm.ConHex;

public class SymTab extends DataLogger {
	
	public SymTab() {
		logInit("symtab",2);
	}
	
	private ArrayList<SymtabRecord> symtab = new ArrayList<SymtabRecord>();
	
	private String trim(String symbol) {
		if ( symbol.endsWith(",X") == true )
			symbol = symbol.replace(",X", "");
		return symbol;
	}

	public void add(String addr, String label, String data) {
		symtab.add(new SymtabRecord(addr, label, data));
	}

	public void add(String addr, Integer label, String data) {
		symtab.add(new SymtabRecord(addr, label.toString(), data));
	}
	
		
	// Contains(String) returns true if argument string is found in symtab "table"
	public Integer getOpSize(String symbol,String operand) {
		symbol=trim(symbol);
		int iPos = contains(symbol);
		if ( iPos != -1 ) {
			return sizeAsm(symbol,symtab.get(iPos).count,operand);
		}
		return -1;
	}
	
	public int contains(String symbol) {
		symbol=trim(symbol);
//		log("Searching optable for symbol : " + symbol);
		for ( int i=0; i < symtab.size(); i++ ) {
			if ( symtab.get(i).label.compareToIgnoreCase(symbol) == 0)
				return i;
		}
		return -1;
	}
	
	public int sizeAsm(String symbol,int count,String operand) {
		
		int labelsize=0;
		
		// Slew of late night dirty hacks being rolled out
		// All RFC's are sent to my mail sorter I fondly call /dev/null
		boolean operandIsString = false;
		
		// if its string, chop off prefix ='  and trailing '
		if ((operand.startsWith("=C'") || operand.startsWith("=c'")) &&
				operand.endsWith("'")) {
			operand = operand.substring(3,operand.length()-1);
			operandIsString = true;
		}
		
		// do the same with Hex literals, except we convert hex back to decimal
		if ((operand.startsWith("=X'") || operand.startsWith("=x'")) &&
				operand.endsWith("'")) {
			operand = operand.substring(3,operand.length()-1);
			operand = ConHex.hexToInt(operand);
		}
		
		// RESW - 3 bytes x N
		if(symbol.equalsIgnoreCase("RESW")) {
			int a = Integer.parseInt(operand);
			labelsize = 3 * a;
		}
		// RESB - 1 byte x N
		else if(symbol.equalsIgnoreCase("RESB")) {
			log("Fuuuuuu");
			int a = Integer.parseInt(operand);
			labelsize = 1 * a;
		}
		// WORD - 3 bytes 
		else if(symbol.equalsIgnoreCase("WORD")) {
			log("Fuuuuuu");
			labelsize = 3;
		}
		// BYTE - 1 byte x N(size) 
		else if(symbol.equalsIgnoreCase("BYTE")) {
			if ( operandIsString == true )
				labelsize = operand.length();
			else {
				labelsize = 1;
			}
		}
		return labelsize;
			
	}
	
	
	// Debug :: Dumps optab to console
	public void dump() {
		log("SymTab contents ("+symtab.size()+")");
		for ( int i=0; i<symtab.size(); i++ )
			log(ConHex.intToHex(symtab.get(i).count) + "\t" + symtab.get(i).label + "\t" + symtab.get(i).operand);
	}
	
	public void update(String label,String address) {
    	
		for ( int i=0; i < symtab.size(); i++ ) {
			if ( symtab.get(i).label.compareToIgnoreCase(label) == 0){
					symtab.get(i).count=Integer.parseInt(address); }
					
		}
		
}


	public String getAddress(String operand) {
		if ( operand == null ) return "0000";
		operand = operand.replace(",X","");
		for ( SymtabRecord iter: symtab) {
			if ( iter.label == null || operand == null )
				continue;
			if ( iter.label.compareToIgnoreCase(operand) == 0) {
				return ConHex.intToHex(iter.count);
			}
		}
		return "0000";
	}
}