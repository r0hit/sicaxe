package emu.sic;

public abstract class DataLogger {

	private StringBuilder data;
	private Integer debugLevel;
	private String prefix;
	private int idx;
	
	public static StringBuilder globalDump = new StringBuilder();
	
	private static String last;
	
	public void logInit(String title, Integer debug) {
		data = new StringBuilder();
		prefix = "[" + title + "] ";
		debugLevel = debug;
		idx = 0;
	}
	
	public String toString() {
		return getData();
	}
	
	public String getData() {
		String out = data.substring(idx);
		idx = idx + out.length();
		return out;
	}
	
	public void log(String s, Integer l) {
		if ( prefix.equals(last) == false )
			System.out.println();
		
		String[] msg = s.split("\n");
		for ( String str: msg ) {
			str = prefix + str + "\n";
			globalDump.append(str);
			data.append(str);
			if ( l <= debugLevel)
				System.out.print(str);
		}
		
		last = prefix;
	}
	
	public void log(String s) {
		log(s, 0);
	}
	
	public void log(Integer i) {
		log(i.toString(), 0);
	}

}
