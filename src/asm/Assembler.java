/*
 * Class: StageOneAsm
 * 
 * Desc:
 *  Stage 1 assembly - Generate addresses
 *   
 */


package asm;

import java.util.ArrayList;
import java.util.Iterator;

import emu.sic.DataLogger;
import arch.sic.AsmDirectives;
import arch.sic.OpTab;
import arch.sic.SymTab;

public class Assembler extends DataLogger {

	public CodeGenerator codegen;
	
	public Assembler() {
		logInit("asmS1", 2);
	}
	
	public void makePassOne(ArrayList<Record> sourceTable, OpTab opTable,
			SymTab sicSymTab, AsmDirectives sicAsmTab) throws Exception {

		//ArrayList<AsmTab> symtab = new ArrayList<AsmTab>();
		// Integer LOCCTR - assign with initial LC value from listing
		Integer locCtr = Integer.parseInt(sourceTable.get(0).operand, 16);
		
		if ( locCtr == null ) {
			locCtr = 0;
		}
		
		// assign same LOCCTR value to first executable(?) instruction
		Iterator<Record> i = sourceTable.iterator();
		Record current = i.next();
		current.count = locCtr;
		
		// Iterate and increment LOCCTR by X bytes every line
		// X = getOpSize from the optab
		
		while ( i.hasNext() ==  true ) {
		    current = i.next();				
		
			if(opTable.getOpSize(current.opcode) != -1) {
				if ( current.label != null )
					sicSymTab.add("0000", current.label,"");
				}
			
			else {
				int temp = sicAsmTab.getSize(current.opcode,current.operand);
				if ( temp != -1 ) {	
					if ( current.operand != null ) {
							
							if(sicSymTab.getOpSize(current.label, current.operand)==-1) {														
								sicSymTab.add("0000", current.label, current.operand); }
							else  {
								log("Multiple Declarations : "+current.label);
								// System.exit(0);
								throw new Exception("Multiple Declarations : "+current.label);
							}						
					}
				
				}
				else
				throw new Exception("Illegal instruction: " + current.opcode);
			}
		}
		
		i=sourceTable.iterator();
		current = i.next();
		while ( i.hasNext() ==  true ) {
		    current = i.next();
		    current.count = locCtr;
		    //log(current.label+" "+current.opcode+" "+current.operand+" "+ConHex.intToHex(locCtr));
		    if(opTable.getOpSize(current.opcode) != -1) {
			    if ( current.operand != null ) {
			    	if ( current.opcode.endsWith(",X") ) {
			    		log("Ends with ,X");
			    		current.opcode = current.opcode.substring(0, current.opcode.length()-3);
			    	}
			    	int res = sicSymTab.contains(current.operand);
			    	if(res==-1){
			    		log("Invalid use of symbol : " + current.operand);
			    		//System.exit(0);
			    		throw new Exception("Invalid use of symbol : " + current.operand);
			    	}
			    }
			    if(current.label!=null) {
			    	sicSymTab.update(current.label,locCtr.toString());
			    }
			    locCtr += opTable.getOpSize(current.opcode); 
			    }
		    else
			    if ( current.label != null && current.operand != null ) {
			    	int temp = sicAsmTab.getSize(current.opcode,current.operand);
			    	if ( temp != -1 ) {	
			    		sicSymTab.update(current.label,locCtr.toString());
			    		locCtr += temp; }
		        
			    }
		}    
	}
	
	public void makePassTwo(ArrayList<Record> sourceTable, OpTab opTable, 
			SymTab sicSymTab, AsmDirectives sicAsmTab) {

		codegen =  new CodeGenerator();
		StringBuilder output = new StringBuilder("");
		ArrayList<String> obcode = new ArrayList<String>();
		String opcode = null;
		String dataOut = null;
		
		Iterator<Record> i = sourceTable.iterator();
		Record current = i.next();
		
		String a = null;
		while ( i.hasNext() ==  true ) {
			dataOut = "";
			
			current = i.next();
		    opcode = opTable.getObCode(current.opcode);	//get opcode from optab
		    // seems like a valid opcode
		   
		    if ( opcode != null ) {
		    	if(current.operand != null && current.operand.endsWith(",X")) {
		    		a = sicSymTab.getAddress(current.operand);
		    		int b = Integer.parseInt(ConHex.hexToInt(a)) | 32768;   		
		    		dataOut=opcode + ConHex.intToHex(b); }
		    	else		    	
		    	dataOut = opcode + sicSymTab.getAddress(current.operand);
		    	
		    } else {
		    	dataOut = sicAsmTab.getData(current.opcode, current.operand);
		    	
		    }
		    output.append(dataOut);
		    obcode.add(dataOut);
		    
		}
		//writeOut(output.toString());
		String out = output.toString();	
		codegen.GenerateCode(out,sourceTable);
		codegen.dump();
	}

}