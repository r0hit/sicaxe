package asm;

public class ConHex {

	public final static int WORD_MAX = 16777215;
	
	//Parse hex address string to decimal(int)
	
	public static String hexToInt(String hexstr) {
		Integer decaddr = Integer.parseInt(hexstr, 16);
		return decaddr.toString();
	}
	
	//Decimal address to Hex string
	public static String intToHex(int decaddr) {
		String hexstr = Integer.toHexString(decaddr);
		return hexstr;
	}
	
	//Hex to Binary
	public static String intToBin(int hexaddr)	{
		Integer binaddr = Integer.parseInt(Integer.toBinaryString(hexaddr));
		return binaddr.toString();
	}
	
	// Pads zeros to data smaller than -size-
	public static String autoPad(String data, Integer size) {
		while ( data.length() < size)
			data = "0" + data;
		return data;
	}
	

	public static int byteArrayToInt(byte[] data) {
		int result, radix, digit;
		result = 0;
		for ( int i = data.length - 1; i >= 0; i-- ) {
			
			radix = (int) Math.pow(256, i);
			digit = (int) (data[i] & 0xFF);
			result += (radix * digit);
		}
		return result;
	}

	public static byte[] intToByteArray(int data) throws NumberFormatException {
		if ( data > WORD_MAX )
			throw new NumberFormatException("Value out of bounds");
		byte[] result = new byte[3];
		int rem;
		for ( int i = 0; data != 0; i++ ) {
			rem = data % 256;
			data = data / 256;
			result[i] = (byte) rem;
		}
		return result;
	}

	public static byte[] intToByteArray(String data) throws NumberFormatException, Exception {
		return intToByteArray(Integer.parseInt(data));
	}
	
	
}