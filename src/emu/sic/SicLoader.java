package emu.sic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class SicLoader extends DataLogger {
	
	SicMem mem = null;
	public String jumpAddress;
	
	public SicLoader(SicMem sicmem) {
		logInit("loader", 2);
		mem = sicmem;
	}
	
	public void loadFile(String objectfile) throws IOException {
		
		
		String temp;
		FileReader input = new FileReader(objectfile);
		BufferedReader br = new BufferedReader(input);
		String readline;
		
		mem.clearall();

		jumpAddress = br.readLine().replace('^',' ').split(" ")[2];
		
		while ( (readline = br.readLine()) != null) {
			if ( readline.startsWith("E^") == true)
				break;
		   temp = "";
		  // String token = "";
		   String[] rec = readline.replace('^',' ').split(" ");
		   for(int i=3;i<rec.length;i++) {
			   temp=temp+rec[i];
		   }
		   mem.blockDump(temp, readline.replace('^',' ').split(" ")[1]);
		}
		br.close();

		//blockDump(row);
		//for(int i=0;i<row.size();i++) {
		//	log(row.get(i));
		//		}
		
	}
	
	public void read(int offset) {
		
	}
	
	public boolean check(int min,int max) {
		return false;
		
	}
	
	
			
			
}
	
	
	
