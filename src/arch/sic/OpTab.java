/* 
 * OpTab (Opcode Table)
 * ====================
 * 
 * Package	: arch/sic
 * 
 * Input	: CSV File = SIC-ISA.csv (static-auto)
 * Output	: OpTab object
 * 
 * Reads and parses CSV file containing ISA for SIC
 * boolean contains(String)
 * 		Returns true if matching opcode is found in optab, false otherwise
 * 
 */

package arch.sic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import emu.sic.DataLogger;

public class OpTab extends DataLogger {

	OpTab() {
		
	}
	// Opcode Table stored in table as ArrayList
	public static ArrayList<OptabRecord> table = new ArrayList<OptabRecord>();
	
	// initialize on object initialization, throw IOException to calling method
	// if ISA load fails
	public OpTab(String filename) throws IOException {
		logInit("optab", 2);
		table.clear();
		try {
			init(filename);
		} catch ( IOException ex ) {
			log("FATAL : Could not load ISA Optabe");
			log(ex.toString());
			throw ex;
		}
		log("Opcode Table initialized");
		log("Symbols read : " + table.size());
	}
	
	
	// Locate SIC-ISA.csv, load it, parse it, dump mnemonic and opcode into 
	// table object ;  IOException generated if file missing
	private void init(String csvfile) throws IOException {
		
		// find file relative to class package path
		URL input = OpTab.class.getResource(csvfile);
		BufferedReader br = new BufferedReader(new FileReader(input.getFile()));
		
		String line;
		line = br.readLine();		// Skip CSV headers
		
		while ( (line = br.readLine()) != null ) {
			line = line.toUpperCase();
			String[] token = line.split(",");
			if ( token.length == 4 ) {
				OptabRecord entry;
				entry = new OptabRecord(token[0], token[1], token[2], token[3]);
				// Add entry into optab object
				table.add(entry);
			}
		}
		br.close();
	}
	
	
	// Contains(String) returns true if argument string is found in optab "table"
	public Integer getOpSize(String symbol) {
		int iPos = contains(symbol);
		if ( iPos != -1 ) {
			return table.get(iPos).count;
		}
		return -1;
	}
	
	// Returns opcode location in OpTab array. Returns -1 if not found
	public int contains(String symbol) {
//		log("Searching optable for symbol : " + symbol);
		for ( int i=0; i < table.size(); i++ ) {
			if ( table.get(i).label.compareToIgnoreCase(symbol) == 0)
				return i;
		}
		return -1;
	}
	
	// Return OpCode from array list for symbol if found, null otherwise
	public String getObCode(String symbol) {
		for ( int i=0; i < table.size(); i++ ) {
			if ( table.get(i).label.compareToIgnoreCase(symbol) == 0)
				return table.get(i).opcode;
		}
		return null;	
	}
	
	
	// Debug :: Dumps optab to console
	public void dump() {
		log("OpTab contents ("+table.size()+")");
		for ( int i=0; i<table.size(); i++ )
			log(table.get(i).label + "\t" + table.get(i).opcode + "\t" + table.get(i).operand, 2);
	}
	
}
