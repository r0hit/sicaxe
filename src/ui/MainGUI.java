/*
 * 
 * Beta UI for SIC Emulation IDE
 * 
 * Swing is such a mess. Looking forward to move to more MVC-compatible UI
 *  toolkit like JavaFX
 * 
 */

package ui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;

import arch.sic.AsmDirectives;
import arch.sic.OpTab;
import arch.sic.SymTab;
import asm.Assembler;
import asm.Record;
import asm.SourceTokenizer;
import emu.sic.SicCpu;
import emu.sic.SicLoader;
import emu.sic.SicMem;

public class MainGUI extends javax.swing.JFrame {

	private static final long serialVersionUID = 1L;
	
	private OpTab 				sicOpTab;
	private SymTab				sicSymTab;
	private SicCpu				sicCpu;
	private SicMem				sicMem;
	private SicLoader			sicLoader;
	private Assembler			sicAsm;
	private AsmDirectives		sicAsmTab;
	private ArrayList<Record>	asmListing;
	
	private void clearWindows() {
		txtMemDump.setText("");
		txtConsole.setText("");
		jTextArea2.setText("");
	}
	
    public void initEmulator() {
    	System.out.println("Reset env");
    	try {
	    	sicOpTab  = new OpTab("SIC-ISA.csv");
	    	sicSymTab = new SymTab();
	    	sicAsm	  = new Assembler();
	    	sicMem    = new SicMem();
	    	sicCpu    = new SicCpu(sicMem);
	    	sicAsmTab = new AsmDirectives();
	    	sicLoader = new SicLoader(sicMem);
	    	sicOpTab.dump();
	    	txtMemDump.append(sicOpTab.getData());
    	} catch (Exception e) {
    		txtConsole.append("Error:\n" + e.toString());
    	}
    }
    
    String nulltab(String s) {
    	if ( s == null )
    		return "";
    	return s;
    }
    
    void assemble() {
    	try {
    		initEmulator();
			sicAsm.makePassOne(asmListing, sicOpTab, sicSymTab, sicAsmTab);
			sicSymTab.dump();
			txtConsole.append(sicSymTab.getData());
			txtConsole.append(sicAsm.getData());
			sicAsm.makePassTwo(asmListing, sicOpTab, sicSymTab, sicAsmTab);
			txtConsole.append(sicAsm.getData());
			txtConsole.append("\nAssembly complete\n");
		} catch (Exception e) {
			// TODO Display error dialogs for exceptions
			System.out.println(e);
		} finally {
			txtConsole.append(sicSymTab.getData());
			txtConsole.append(sicAsm.getData());
			for (String line: sicAsm.codegen.records)
				jTextArea2.append(line + "\n");
			tabViews.setSelectedIndex(1);
		}
    }
    
    void showListing() {
    	String temp;
      	jTextArea1.setText("");
		for ( int i = 0; i < asmListing.size(); i++) {
			temp = nulltab(asmListing.get(i).label) + "\t" +
					nulltab(asmListing.get(i).opcode) + "\t" +
					nulltab(asmListing.get(i).operand) + "\n";
			jTextArea1.append(temp);
		}
    }
    
    void autoFormat() {
    	
    	try {
    		
	    	FileOutputStream tmpfile = new FileOutputStream("tmp.sic");
	    	tmpfile.write(jTextArea1.getText().getBytes());
	    	tmpfile.close();
	      	asmListing = SourceTokenizer.parseSourceFile("tmp.sic");
	    	
	      	showListing();
	      	
    	} catch (Exception ex) {
    		JOptionPane.showMessageDialog(this, ex.toString(), "Error",
    				JOptionPane.ERROR_MESSAGE, null);
    	}
    }
    
    void openFile(File src) {
    	try {
      	  	asmListing = SourceTokenizer.parseSourceFile(src.getAbsolutePath());
      	  	showListing();
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(this, ex.toString(), "Error", 
					JOptionPane.ERROR_MESSAGE, null);
		}
    }
    
    void refreshRegTable() {
    	registerTable.setModel(new javax.swing.table.DefaultTableModel(
                sicCpu.getSnapshot(),
                new String [] {
                    "Register", "Value"
                }
            ) {
    			private static final long serialVersionUID = 1L;
    			boolean[] canEdit = new boolean [] {
                    false, false
                };

                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit [columnIndex];
                }
            });
    }
    
    private void initComponents() {

    	setTitle("SIC Assembler/Emulator");
    	String resource = MainGUI.class.getResource("/ui/resource").toString().
    			replace("file:", "");
    	
    	btnNew = new javax.swing.JButton("New", 
    			new ImageIcon(resource + "/document-new.png"));
        btnOpen = new javax.swing.JButton("Open", 
        		new ImageIcon(resource + "/document-open.png"));
        btnAuto = new javax.swing.JButton("AutoFormat", 
        		new ImageIcon(resource + "/dialog-ok.png"));
        btnAssemble = new javax.swing.JButton("Assemble", 
        		new ImageIcon(resource + "/system-run.png"));
        btnLoad = new javax.swing.JButton("Load", 
        		new ImageIcon(resource + "/extract-archive.png"));
        btnReset = new javax.swing.JButton("Reset", 
        		new ImageIcon(resource + "/dialog-apply.png"));
        btnRun = new javax.swing.JButton("Step", 
        		new ImageIcon(resource + "/go-last.png"));
        
        rootSplitPane = new javax.swing.JSplitPane();
        rightSplitPane = new javax.swing.JSplitPane();
        memDumpPane = new javax.swing.JScrollPane();
        txtMemDump = new javax.swing.JTextArea();
        registerTablePane = new javax.swing.JScrollPane();
        registerTable = new javax.swing.JTable();
        leftSplitPane = new javax.swing.JSplitPane();
        tabViews = new javax.swing.JTabbedPane();
        viewTab1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        viewTab2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        consoleScrollPane = new javax.swing.JScrollPane();
        txtConsole = new javax.swing.JTextArea();
        toolbarMain = new ui.JSkinnableToolbar("MainToolbar", 
        		JToolBar.HORIZONTAL, new ImageIcon(resource + "/tb_bg.png"));
        jSeparator1 = new javax.swing.JToolBar.Separator();
        jSeparator2 = new javax.swing.JToolBar.Separator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(800, 480));
        setPreferredSize(new java.awt.Dimension(900, 480));

        rootSplitPane.setDividerLocation(640);
        rootSplitPane.setDividerSize(5);

        rightSplitPane.setDividerLocation(150);
        rightSplitPane.setDividerSize(5);
        rightSplitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        txtMemDump.setColumns(20);
        txtMemDump.setRows(5);
        memDumpPane.setViewportView(txtMemDump);

        rightSplitPane.setBottomComponent(memDumpPane);

        
        registerTablePane.setViewportView(registerTable);
        if (registerTable.getColumnModel().getColumnCount() > 0) {
            registerTable.getColumnModel().getColumn(0).setResizable(false);
            registerTable.getColumnModel().getColumn(1).setResizable(false);
        }

        rightSplitPane.setLeftComponent(registerTablePane);

        rootSplitPane.setRightComponent(rightSplitPane);

        leftSplitPane.setDividerLocation(300);
        leftSplitPane.setDividerSize(5);
        leftSplitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        tabViews.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);

        viewTab1.setLayout(new java.awt.BorderLayout());

        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        viewTab1.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        tabViews.addTab("Source Listing", viewTab1);

        viewTab2.setLayout(new java.awt.BorderLayout());

        jTextArea2.setColumns(20);
        jTextArea2.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jTextArea2.setRows(5);
        jTextArea2.setEnabled(false);
        jScrollPane2.setViewportView(jTextArea2);

        viewTab2.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        tabViews.addTab("Object Code", viewTab2);

        leftSplitPane.setTopComponent(tabViews);

        txtConsole.setColumns(20);
        txtConsole.setRows(5);
        consoleScrollPane.setViewportView(txtConsole);
        
        txtConsole.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        txtMemDump.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
         
        leftSplitPane.setRightComponent(consoleScrollPane);

        rootSplitPane.setLeftComponent(leftSplitPane);

        getContentPane().add(rootSplitPane, java.awt.BorderLayout.CENTER);

        toolbarMain.setRollover(true);
  
        btnNew.setFocusable(false);
        btnNew.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnNew.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	int reply = 0;
            	if (jTextArea1.getText() != "") {
            		reply = JOptionPane.showConfirmDialog(null, 
            				"Discard current program?", "New file", 
            				JOptionPane.YES_NO_OPTION);
            	}
            	if ( reply == JOptionPane.YES_OPTION) {
            		jTextArea1.setText("");
            	}
            }
        });
        toolbarMain.add(btnNew);

        
        btnOpen.setFocusable(false);
        btnOpen.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnOpen.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);
        btnOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	int reply = 0;
            	if (jTextArea1.getText() != "") {
            		reply = JOptionPane.showConfirmDialog(null, 
            				"Discard current program?", "Open existing file", 
            				JOptionPane.YES_NO_OPTION);
            	}
            	if ( reply == JOptionPane.YES_OPTION) {
	            	System.out.println(evt.getClass().toString());
	            	JFileChooser chooser = new JFileChooser();
	                chooser.setMultiSelectionEnabled(false);
	                int option = chooser.showOpenDialog(null);
	                if (option == JFileChooser.APPROVE_OPTION) {
	                  openFile(chooser.getSelectedFile());
	                }
            	}
            }
        });
        toolbarMain.add(btnOpen);

        btnAuto.setFocusable(false);
        btnAuto.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnAuto.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAuto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
               autoFormat();
            }
        });
        toolbarMain.add(btnAuto);
        toolbarMain.add(jSeparator1);

        btnAssemble.setFocusable(false);
        btnAssemble.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnAssemble.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAssemble.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	clearWindows();
            	autoFormat();
            	assemble();
            }
        });
        toolbarMain.add(btnAssemble);

        btnLoad.setFocusable(false);
        btnLoad.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnLoad.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);
        btnLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	try {
            		sicLoader.loadFile("object");
            		txtConsole.append("\nObject code loaded\n");
            		sicCpu.init(sicLoader.jumpAddress);
            		txtConsole.append(sicCpu.getData());
            		refreshRegTable();
            	} catch (IOException e) {
            		txtConsole.append("\nError:\n" + e.toString() + "\n");
            	}
            }
        });
        toolbarMain.add(btnLoad);
        toolbarMain.add(jSeparator2);

        btnReset.setFocusable(false);
        btnReset.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnReset.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	clearWindows();
                sicCpu.reset();
                refreshRegTable();
            	txtMemDump.setText(sicCpu.getData());
            }
        });
        toolbarMain.add(btnReset);

        btnRun.setFocusable(false);
        btnRun.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnRun.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	try {
					sicCpu.fetch();
					refreshRegTable();
				} catch (Exception e) {
					txtConsole.append(e.toString());
					e.printStackTrace();
				} finally {
					txtConsole.append(sicCpu.getData());
				}
            }
         });
        toolbarMain.add(btnRun);

        getContentPane().add(toolbarMain, java.awt.BorderLayout.PAGE_START);

        pack();
    }

    
    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.
            		UIManager.getInstalledLookAndFeels()) {
                if ("GTK+".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                }
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MainGUI.class.getName()).log(
            		java.util.logging.Level.SEVERE, null, ex);
        }

        /* Create and display the form */
        new MainGUI().setVisible(true);
    }

    public MainGUI() {
        initComponents();
    }

    // Variables declaration - do not modify  -  once upon a time said NetBeans

    // toolbar ////////////////////////////////////////////////////////////////
    private ui.JSkinnableToolbar toolbarMain;
    
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator2;
    
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnOpen;
    private javax.swing.JButton btnAuto;
    private javax.swing.JButton btnLoad;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnRun;
    private javax.swing.JButton btnAssemble;

    // root window /////////////////////////////////////////////////////////////
    private javax.swing.JSplitPane rootSplitPane;
    private javax.swing.JSplitPane leftSplitPane;
    private javax.swing.JSplitPane rightSplitPane;


    // tab views ///////////////////////////////////////////////////////////////
    
    private javax.swing.JTabbedPane tabViews;

    private javax.swing.JPanel viewTab1;
    private javax.swing.JPanel viewTab2;

    // components //////////////////////////////////////////////////////////////
    private javax.swing.JTable registerTable;
    
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextArea txtMemDump;
    private javax.swing.JTextArea txtConsole;
     
    private javax.swing.JScrollPane memDumpPane;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane consoleScrollPane;
    private javax.swing.JScrollPane registerTablePane;
    

    // End of variables declaration                   
}
