package arch.sic;

import asm.Record;

//General SIC Instruction structure-type
public class OptabRecord extends Record {
	
	OptabRecord(String mnemonic, String args, String hexcode, String size) {
		label = mnemonic;
		operand = args;
		opcode = hexcode;
		count = Integer.parseInt(size);	 // Reuse field for instruction length
	}
	
}