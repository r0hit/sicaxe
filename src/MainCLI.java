import java.util.ArrayList;

import emu.sic.SicCpu;
import emu.sic.SicLoader;
import emu.sic.SicMem;
import asm.*;
import arch.sic.*;

public class MainCLI {
	
	public static void main(String[] args) throws Exception {

		String sourcecode = "test3.txt";
				
		// ArrayList object to contain tokenized listing file we can work with
		ArrayList<Record> asmListing = new ArrayList<Record>();
		
		// SIC machine OpTab object (arch.sic.SIC-ISA.csv contains optab data)
		// Supposed to be upwards compatible for SIC-XE - new SICXE-ISA.csv file
		OpTab sicOpTab = new OpTab("/arch/sic/SIC-ISA.csv");
		SymTab sicSymTab = new SymTab();					// symtab object
		AsmDirectives sicAsmTab = new AsmDirectives();	// assembler directives 

		// parse assembly listing text file
		asmListing = SourceTokenizer.parseSourceFile(sourcecode);
		
		sicOpTab.dump();

		// run passes
		Assembler sicAsm = new Assembler();
		sicAsm.makePassOne(asmListing, sicOpTab, sicSymTab, sicAsmTab);

		sicSymTab.dump();
		System.out.println("Source Code :");
		for ( Record r: asmListing)
			System.out.println(r);
		
		sicAsm.makePassTwo(asmListing, sicOpTab, sicSymTab, sicAsmTab);

		SicMem memory = new SicMem();
		
		SicLoader r = new SicLoader(memory);
		r.loadFile("object");
		
		memory.dump("1000", "1078");
		
		SicCpu cpu = new SicCpu(memory);
		cpu.init("1000");
		cpu.fetch();
		cpu.fetch();
		cpu.fetch();
		cpu.fetch();
		cpu.fetch();
		cpu.fetch();
		cpu.fetch();
		cpu.fetch();
		cpu.fetch();
		cpu.fetch();

		memory.dump("1000", "1078");

		//cpu.start();

	}
}
