package emu.sic;

import asm.ConHex;

public class SicCpu extends DataLogger implements SicISA {
	
	// CPU Register set
	byte[] a, x, l, pc, sw;
	
	// RAM reference object
	SicMem mem = null;

	public Object[][] getSnapshot() {
		if ( a == null )
			return null;
		Object[][] registers = {
				{"Accumulator", 	 ConHex.intToHex(ConHex.byteArrayToInt(a)) },
				{"Program Counter",  ConHex.intToHex(ConHex.byteArrayToInt(pc))},
				{"Linkage Register", ConHex.intToHex(ConHex.byteArrayToInt(l)) },
				{"Index Register", 	 ConHex.intToHex(ConHex.byteArrayToInt(x)) },
				{"Status Word",		 ConHex.intToHex(ConHex.byteArrayToInt(sw))}
		};
		return registers;
	}
	
	// Constructors to set internal memory object reference
	public SicCpu(SicMem sicmem) {
		logInit("emucpu", 2);
		log("SIC CPU Emulator; Version 0.1 - alpha", 1);
		log("Diagnostics message level 2" , 1);
		mem = sicmem;
	}
	
	public SicCpu() {
		logInit("emucpu", 2);
		log("ERROR : CPU not functional. Missing memory object", 0);
	}
	
	// CPU Reset - erase all registers
	public void reset() {
		log("CPU Reset", 1);
		log("Allocating memory for registers", 2);
		a = new byte[3];
		x = new byte[3];
		l = new byte[3];
		pc = new byte[3];
		sw = new byte[3];
	}
	
	// Initialize CPU (reset and set PC)
	public void init(String address) {
		init(Integer.parseInt(ConHex.hexToInt(address)));
	}
	public void init(int address) {
		reset();
		pc = ConHex.intToByteArray(address);
		log("Set PC = " + ConHex.byteArrayToInt(pc), 2);
	}
	
	
	// --> CPU INTERNALS
	// Instruction FETCH
	// Fetch instruction from memory
	public void fetch() throws Exception {
		byte[] data = mem.getWord(ConHex.byteArrayToInt(pc));
		byte[] addr = new byte[2];
		addr[1] = data[1];
		addr[0] = data[0];
		
		log("[PC @ 0x" + ConHex.intToHex(ConHex.byteArrayToInt(pc)) + "]", 3);
		// increment PC
		int pc_next = ConHex.byteArrayToInt(pc) + 3;
		pc = ConHex.intToByteArray(pc_next);
		decode(data[2], addr);
	}
	
	
	// Instruction DECODE & EXECUTE cycle
	public void decode(byte instruction, byte[] address) throws Exception {
		// decode 
		switch (instruction) {
			case 0x00 : lda(address); break;
			case 0x04 : ldx(address); break;
			case 0x08 : ldl(address); break;
			case 0x0c : sta(address); break;
			case 0x10 : stx(address); break;
			case 0x14 : stl(address); break;
			case 0x18 : add(address); break;
			case 0x1c : sub(address); break;
			case 0x20 : mul(address); break;
			case 0x24 : div(address); break;
			case 0x28 : comp(address); break;
			case 0x2c : tix(address); break;
			case 0x30 : jeq(address); break;
			case 0x34 : jgt(address); break;
			case 0x38 : jlt(address); break;
			case 0x3c : j(address); break;
			case 0x44 : or(address); break;
			case 0x48 : jsub(address); break;
			case 0x4c : rsub(address); break;
			case 0x50 : ldch(address); break;
			case 0x54 : stch(address); break;
			case 0x58 : and(address); break;
			case (byte) 0xd8 : rd(address); break;
			case (byte) 0xdc : wd(address); break;
			case (byte) 0xe0 : td(address); break;
			case (byte) 0xe8 : stsw(address); break;
			default: throw new Exception("Illegal instruction <0x" 
						+ ConHex.intToHex(instruction) + "> at " 
						+ ConHex.intToHex(ConHex.byteArrayToInt(address)));
		}
	}
	
	// meh
	public void execute() {	}
	public void startExec() { }
	public void nextStep() { }
	public void setBreakpoint(int[] addr) { }
	public void resume() { }
	
	// method used to get Effective Address(EA) of given address
	//  calculates absolute address fpr indexed addressing, else returns same 
	private int getEA(byte[] address) {
		int ea = 0;
		if ((address[1] & 0b10000000) == 0b10000000) {	// if index addressing
			address[1] &= 0b01111111;					// remove indexed(X) bit
			ea = ConHex.byteArrayToInt(x);				// assign X to EA
		} 
		// add absolute address to EA
		ea += ConHex.byteArrayToInt(address);
		return ea;
	}
	
	/* ---> SIC ISA Implementation in Java
	 * 
	 * DONE :
	 * add, lda, sta, and, or, sub, mul, div, comp, j, jeq, jlt, jgt, ldch, ldl,
	 * ldx, stch, stl, stsw, stx, tix, jsub, rsub
	 * 
	 * PENDING :
	 * rd, wd, td
	 * 
	 * UNSUPPORTED/WONTFIX :
	 * rd, wd, td
	 */
	
	
	@Override
	public void add (byte[] address) {
		int accvalue = ConHex.byteArrayToInt(a);
		int ea = getEA(address);

		int memvalue = ConHex.byteArrayToInt(mem.getWord(ea));
		log("Add A " + accvalue + " + " + memvalue, 2);
		accvalue += memvalue;
		a = ConHex.intToByteArray(accvalue);
	}

	@Override
	public void and (byte[] address) {
		int ea = getEA(address);
		byte[] data = mem.getWord(ea);
		a[0] = (byte) (a[0] & data[0]);
		a[1] = (byte) (a[1] & data[1]);
		a[2] = (byte) (a[2] & data[2]);
		log("AND (A, [" + ea + "]) = " + ConHex.byteArrayToInt(a), 2);
	}

	@Override
	public void comp (byte[] address) {
		int accvalue = ConHex.byteArrayToInt(a);
		int ea = getEA(address);

		int memvalue = ConHex.byteArrayToInt(mem.getWord(ea));
		log("Comp " + memvalue + " with " + accvalue, 2);
		if ( accvalue == memvalue )
			sw[0] = '=';
		else if ( accvalue < memvalue )
			sw[0] = '<';
		else if ( accvalue > memvalue )
			sw[0] = '>';
		 
		
	}

	@Override
	public void div (byte[] address) {	// avoid creating DivByZero blackholes
		int accvalue = ConHex.byteArrayToInt(a);
		int ea = getEA(address);

		int memvalue = ConHex.byteArrayToInt(mem.getWord(ea));
	
		if ( memvalue == 0 ) {
			log("Divide by zero exception", 0);
			throw new ArithmeticException();
		}
		
		log("Div A " + accvalue + " / " + memvalue, 2);
		accvalue /= memvalue;
		a = ConHex.intToByteArray(accvalue);
	}

	@Override
	public void j (byte[] address) {
		int ea = getEA(address);
		int memvalue = ConHex.byteArrayToInt(mem.getWord(ea));
		pc = ConHex.intToByteArray(memvalue);
		log("J >> " + ConHex.intToHex(ea), 2);
	}

	@Override
	public void jeq (byte[] address) {
		if ( sw[0] == '=' )
			j(address);
	}

	@Override
	public void jgt (byte[] address) {
		if ( sw[0] == '>' )
			j(address);
	}

	@Override
	public void jlt (byte[] address) {
		if ( sw[0] == '<' )
			j(address);
	}

	@Override
	public void jsub (byte[] address) {
		l[0] = pc[0];
		l[1] = pc[1];
		l[2] = pc[2];
		pc = mem.getWord(ConHex.byteArrayToInt(address));
		log("JSUB, L<-PC; PC<-0x" + ConHex.intToHex(ConHex.byteArrayToInt(pc)), 2);
	}

	@Override
	public void lda (byte[] address) {
		int memaddr = getEA(address);
		a = mem.getWord(memaddr);
		log("Load A [" + memaddr + "] = " + ConHex.byteArrayToInt(a), 2);
	}

	@Override
	public void ldch (byte[] address) {
		int memaddr = getEA(address);		
		a[0]=a[1]=a[2] = 0;
		a[0] = (mem.getByte(memaddr));
		log("Load char A [" + memaddr + "] = " + a[0], 2);
	}

	@Override
	public void ldl (byte[] address) {
		int memaddr = getEA(address);		
		l = (mem.getWord(memaddr));
		log("Load L [" + memaddr + "] = " + ConHex.byteArrayToInt(a), 2);
	}

	@Override
	public void ldx (byte[] address) {
		int memaddr = getEA(address);
		x = (mem.getWord(memaddr));
		log("Load X [" + memaddr + "] = " + ConHex.byteArrayToInt(a), 2);
	}

	@Override
	public void mul (byte[] address) {
		int accvalue = ConHex.byteArrayToInt(a);
		int ea = getEA(address);
		int memvalue = ConHex.byteArrayToInt(mem.getWord(ea));
		log("Mul A " + accvalue + " * " + memvalue, 2);
		accvalue *= memvalue;
		a = ConHex.intToByteArray(accvalue);
	}

	@Override
	public void or (byte[] address) {
		int ea = getEA(address);
		byte[] data = mem.getWord(ea);
		a[0] = (byte) (a[0] | data[0]);
		a[1] = (byte) (a[1] | data[1]);
		a[2] = (byte) (a[2] | data[2]);
		log("OR (A, [" + ea + "]) = " + ConHex.byteArrayToInt(a), 2);
	}
	
	@Override
	public void rd (byte[] address) {
		log("Do RD", 2);
	}

	@Override
	public void rsub (byte[] address) {
		pc[0] = l[0];
		pc[1] = l[1];
		pc[2] = l[2];
		log("RSUB  PC <- L [0x" + ConHex.intToHex(ConHex.byteArrayToInt(l)), 2);
	}

	@Override
	public void sta (byte[] address) {
		int ea = getEA(address);
		mem.setWord(a, ea);
		log("STA " + ConHex.byteArrayToInt(a) + " >> " + ea, 2);
	}

	@Override
	public void stch (byte[] address) {
		int ea = getEA(address);
		mem.setByte(a[0], ea);
		log("STCH " + ConHex.byteArrayToInt(a) + " >> " + ea, 2);
	}

	@Override
	public void stl (byte[] address) {
		int ea = getEA(address);
		mem.setWord(l, ea);
		log("STL " + ConHex.byteArrayToInt(a) + " >> " + ea, 2);
	}

	@Override
	public void stsw (byte[] address) {
		int ea = getEA(address);
		mem.setWord(sw, ea);
		log("STSW " + ConHex.byteArrayToInt(a) + " >> " + ea, 2);
	}

	@Override
	public void stx (byte[] address) {
		int ea = getEA(address);
		mem.setWord(x, ea);
		log("STX " + ConHex.byteArrayToInt(a) + " >> " + ea, 2);
	}

	@Override
	public void sub (byte[] address) {
		int accvalue = ConHex.byteArrayToInt(a);
		int ea = getEA(address);
		int memvalue = ConHex.byteArrayToInt(mem.getWord(ea));
		log("Sub A " + accvalue + " - " + memvalue, 2);
		accvalue -= memvalue;
		a = ConHex.intToByteArray(accvalue);
	}

	@Override
	public void td (byte[] address) {
		log("Do TD", 2);
	}

	@Override
	public void tix (byte[] address) {
		comp(address);
		int idxvalue = ConHex.byteArrayToInt(x);
		idxvalue += 1;
		x = ConHex.intToByteArray(idxvalue);
		log("TIX  X:" + idxvalue, 2);
	}

	@Override
	public void wd (byte[] address) {
		log("Do WD", 2);
	}

}