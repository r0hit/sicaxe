/* 
 * (Source)Record
 * ====================
 * 
 * Package	: asm
 * 
 * Input	: N/A  (DataType)
 * Output	: N/A  (DataType)
 * 
 * DataStructure to hold 3 strings of an assembly statement.
 * toString() method overloaded for SOP() friendliness.
 * Meant to be used as ArrayList generic type.
 */

package asm;

public class Record {
	public Integer count = 0;
	public String label;
	public String opcode;
	public String operand;
	
	public String toString() {
		String templabel, tempoperand;
		if ( label == null )
			templabel = "";
		else
			templabel = label;
		
		
		if ( operand == null )
			tempoperand = "";
		else
			tempoperand = operand;
		
		return asm.ConHex.intToHex(count)+"\t"+templabel+"\t"+opcode+"\t"+tempoperand;
	}
	
}