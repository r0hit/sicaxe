package asm;

import java.io.FileWriter;
import java.util.ArrayList;

import emu.sic.DataLogger;

public class CodeGenerator extends DataLogger {
	
	String[] obcodes;
	public ArrayList<String> records = new ArrayList<String>();
	
	void writeOut(String data) {
		log(data);
	}	
	
	CodeGenerator() {
		logInit("codegen", 2);
	}
	
	public void GenerateCode(String out,ArrayList<Record> sourceTable) {
		
		StringBuilder output = new StringBuilder("");
		
		Integer startAddr = sourceTable.get(0).count;
		Integer endAddr =0;
		if(sourceTable.get(sourceTable.size()-1).opcode.equalsIgnoreCase("RESW"))
			endAddr= sourceTable.get(sourceTable.size()-1).count+ (3 * Integer.parseInt((sourceTable.get(sourceTable.size()-1).operand)));
		else if(sourceTable.get(sourceTable.size()-1).opcode.equalsIgnoreCase("RESB"))
			endAddr= sourceTable.get(sourceTable.size()-1).count+ Integer.parseInt((sourceTable.get(sourceTable.size()-1).operand));
		else if(sourceTable.get(sourceTable.size()-1).opcode.equalsIgnoreCase("WORD"))
			endAddr= sourceTable.get(sourceTable.size()-1).count+ 3;
		else if(sourceTable.get(sourceTable.size()-1).opcode.equalsIgnoreCase("BYTE"))
			endAddr= sourceTable.get(sourceTable.size()-1).count+ 1;
		else 
			endAddr = sourceTable.get(sourceTable.size()-1).count;
		
				
		
		writeOut("Object code generation");
		records.add("H^" + sourceTable.get(0).label + "^" + sourceTable.get(0).operand + "^" + ConHex.intToHex(endAddr - startAddr));
				
		obcodes = out.split("(?<=\\G.{6})");
				
		int size =0;		
		int count = 0;
		int addr=sourceTable.get(1).count;	
		for ( int j = 0; j < obcodes.length; j++) {		
			output.append("^"+obcodes[j]);
			size=size+obcodes[j].length();
			count++;
			if ( count == 10 || j == (obcodes.length -1) ) {
				count = 0;
				size=size/2;
				records.add("T^"+ConHex.autoPad(ConHex.intToHex(addr),6)+"^"+ConHex.intToHex(size)+output.toString());
				output.setLength(0);
				size =0;
				addr=addr+30;
			}
		}
		records.add("E^"+ sourceTable.get(0).operand);
	}
	
	public void dump() {
		try {
			ArrayList<String> obrecs = records;
			FileWriter fout= new FileWriter("object");
			//BufferedWriter bw = new BufferedWriter(fout);
			for(int i=0;i<obrecs.size();i++) {
				writeOut(obrecs.get(i));
			fout.write(obrecs.get(i));
			fout.write("\n");
		}
		fout.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
}