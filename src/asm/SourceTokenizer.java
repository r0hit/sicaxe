/* 
 * SourceTokenizer
 * ====================
 * 
 * Package	: asm
 * 
 * Input	: void openFile(String)
 * Output	: ArrayList<Record> source
 * 
 * Tokenize input file, trims leading/trailing/intermediate white spaces
 * 3-column result stored in publicly accessible source object
 */

package asm;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class SourceTokenizer {
	
	
	private static FileReader input;

	public static ArrayList<Record> parseSourceFile(String path) throws FileNotFoundException, IOException, Exception {


		// output source table. See asm.Record
		ArrayList<Record> source = new ArrayList<Record>();
			
		// clear source table if new file is loaded/re-loaded
		source.clear();
		
		input = new FileReader(path);
		// to store valid tokens from read line
		ArrayList<String> row = new ArrayList<String>();
		
		String readline;
		int linecount = 0;
		BufferedReader br = new BufferedReader(input);
		while ( (readline = br.readLine()) != null) {

			row.clear();
			
			// Replace tab-space with white-space
			readline = readline.replaceAll("\t", " ");
			
			// Explode string, store tokens in array
			String[] tokens = readline.split(" ");
			
			// add only non-null strings to row array
			for ( int i = 0 ; i < tokens.length; i++) {
				if ( tokens[i].isEmpty() == false) {
					if ( tokens[i].startsWith(";") ) {
						break;
					}
					row.add(tokens[i].toUpperCase());
				}
			}
			
			if ( row.size() == 1 ) {
				// Single operand instruction support
				Record r1 = new Record();
				r1.label = null;
				r1.opcode = row.get(0);
				r1.operand = null;
				source.add(r1);
			}
			if ( row.size() == 2 ) {
				// Two tokens found; set address to null
				Record r1 = new Record();
				r1.label = null;
				r1.opcode = row.get(0);
				r1.operand = row.get(1);
				source.add(r1);
			
			} else if ( row.size() == 3 ) {
				// Populate address, opcode, operand and push
				Record r1 = new Record();
				r1.label = row.get(0);
				r1.opcode = row.get(1);
				r1.operand = row.get(2);
				source.add(r1);
				
			} else if (row.size() > 3 ) {
				// 
				throw new Exception("Invalid coding format at line #" +
						(++linecount)+ "\n > Line: " + readline + 
						"\n > Tokens: "+ row.size());
			}
			
		}
		new BufferedReader(input).close();
		input.close();
		input = null;
		
		return source;
	}
}
