package emu.sic;

// Blank interface just so that nothing is missed, intentionally or unintentionally

interface SicISA {
	void lda(byte[] address);
	void ldx(byte[] address);
	void ldl(byte[] address);
	void sta(byte[] address);
	void stx(byte[] address);
	void stl(byte[] address);
	void add(byte[] address);
	void sub(byte[] address);
	void mul(byte[] address);
	void div(byte[] address);
	void comp(byte[] address);
	void tix(byte[] address);
	void jeq(byte[] address);
	void jgt(byte[] address);
	void jlt(byte[] address);
	void j(byte[] address);
	void or(byte[] address);
	void jsub(byte[] address);
	void rsub(byte[] address);
	void ldch(byte[] address);
	void stch(byte[] address);
	void and(byte[] address);
	void rd(byte[] address);
	void wd(byte[] address);
	void td(byte[] address);
	void stsw(byte[] address);
}
