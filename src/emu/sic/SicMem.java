package emu.sic;

import asm.ConHex;

public class SicMem extends DataLogger {
	
	private final static int maxmem = 32768;
	private byte[] memarray = new byte[maxmem];
	
	public SicMem() {
		logInit("sicmem", 2);
	}
	
	// compatibility wrapper
	private int hexToInt(String addr) {
		return Integer.parseInt(asm.ConHex.hexToInt(addr));
	}
	
	/* absolute integer addressing methods */
	public byte getByte(int addr) {
		return (byte) (memarray[addr] & 0xFF);
	}

	public void setByte(int data, int addr) {
		memarray[addr] = (byte) data;
	}
	
	public byte[] getWord(int addr) {
		byte[] word = new byte[3];
		word[2] = getByte(addr);
		word[1] = getByte(addr+1);
		word[0] = getByte(addr+2);
		return word;
	}
	
	public void setWord(byte[] data, int addr) {
		memarray[addr] = data[2];
		memarray[addr+1] = data[1];
		memarray[addr+2] = data[0];
	}
	
	public void blockDump(String data, int addr) {
		String oneByte = "";
		int i;
		for ( i = 0; i < data.length(); i+=2 ) {
			oneByte = data.substring(i, i+2);
			memarray[addr++] = (byte) Integer.parseInt(ConHex.hexToInt(oneByte));	
		}
		//log("Dumped " + i + " nibbles");
	}
	
	/* hexadecimal addressing wrapper methods */
	public byte getByte(String addr) {
		return getByte(hexToInt(addr));
	}
	
	public void setByte(byte data, String addr) {
		setByte(data, hexToInt(addr));
	}
	
	public byte[] getWord(String addr) {
		return getWord(hexToInt(addr));
	}
	
	public void setWord(byte[] data, String addr) {
		setWord(data, hexToInt(addr));
	}
	
	public void blockDump(String data, String addr) {
		blockDump(data, hexToInt(addr));
	}
	/* general functions */
	public void clearall() {
		memarray = new byte[maxmem];
	}
	
	/* debug - devs only */
	public void dump(int start, int end) {
		log("Memory dump [decimal addresses] (" + start + "," + end + ")");
		int wordsize = 0;
		int breakat = 0;
		StringBuilder str = new StringBuilder("");
		
		str.append(ConHex.autoPad(asm.ConHex.intToHex(start), 4) + " : ");
		for ( int j = start; j <= end; j++ ) {
			str.append(ConHex.autoPad(ConHex.intToHex(memarray[j] & 0xFF), 2));
			wordsize+=2;
			if ( wordsize >=6 ) {
				str.append(" ");
				wordsize = 0;
				breakat++;
			}
			if ( breakat >= 10 ) {
				breakat = 0;
				str.append("\n" + ConHex.autoPad(asm.ConHex.intToHex(j+1), 4) + " : ");
			}
		}
		
		log(str.toString());
	}
	
	public void dump(String start, String end) {
		int min = hexToInt(start);
		int max = hexToInt(end);
		dump(min, max);
	}
	
}
