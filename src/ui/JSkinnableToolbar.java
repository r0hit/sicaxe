/**
 * 
 */
package ui;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JToolBar;

public class JSkinnableToolbar extends JToolBar {

	private static final long serialVersionUID = 1L;

	private ImageIcon bgImage;
    
	public JSkinnableToolbar() {
	}

	public JSkinnableToolbar(int orientation) {
		super(orientation);
	}

	public JSkinnableToolbar(String name) {
		super(name);
	}

	public JSkinnableToolbar(String name, int orientation, ImageIcon ii) {
		super(name, orientation);
        this.bgImage = ii;
        setOpaque(true);
	}
	
	public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (bgImage != null) {
           Dimension size = this.getSize();
           g.drawImage(bgImage.getImage(), 0,0, size.width, size.height, this);
        }
     }

}
