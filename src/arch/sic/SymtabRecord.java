package arch.sic;

import asm.Record;

//General SIC Symbol table def
public class SymtabRecord extends Record {

	SymtabRecord(String address, String arglabel, String data) {
		label = arglabel;
		operand = data;
		count = Integer.parseInt(address);	 // Reuse field for instruction length
	}
	
	
}