
package unitTests;

import static org.junit.Assert.*;
import org.junit.Test;
import asm.ConHex;

/**
 * @author rohit
 *
 * Code shamelessly stolen from http://www.vogella.com/articles/JUnit/article.html
 * Section 2.3
 *  
 */
public class ConHexTest {

	// 10 sec timeout, increase timeout for slower CPU's
	@Test(timeout=100000)
	public void verifyRange() {

		// sanity check
		System.out.println("\nTesting sanity of byte to int converters");
		byte[] data = null;

		int check;
		
		for ( int i = 0; i <= ConHex.WORD_MAX; i++ ) {
			try {
				data = ConHex.intToByteArray(i);
				check = ConHex.byteArrayToInt(data);
				assertTrue ( i == check );
				//System.err.println(" > " + data[2] +","+ data[1] +"," + data[0] + "\t\t" + i + "\t" + check);
			} catch ( Exception e ) {
				fail ("exception generated");
				// e.printStackTrace();
			}
			
		}
		
		System.out.println("GOOD");
	}
	

	@Test
	public void minmax() {
		byte[] x = null;
		int value;
		
		System.out.println("Checking for min, max values (known)");
		// min value
		value = 0;
		x = ConHex.intToByteArray(value);
		assertTrue (x[2] == 0 && x[1] == 0 && x[0] == 0);
		
		// max value
		value = ConHex.WORD_MAX;
		x = ConHex.intToByteArray(value);
		assertTrue (x[2] == -1 && x[1] == -1 && x[0] == -1);
		
		System.out.println("GOOD");
	}
	
	

}
