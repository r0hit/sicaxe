package arch.sic;

import asm.Record;

//General SIC Symbol Table structure-type
public class AsmTabRecord extends Record {

	public AsmTabRecord(String mnemonic, String args, int size) {
		label = mnemonic;
		operand = args;
		count = size;	 // Reuse field for instruction length
	}
	
	public AsmTabRecord(String mnemonic, int size) {
		label = mnemonic;
		operand = null;
		count = size;	 // Reuse field for instruction length
	}
}