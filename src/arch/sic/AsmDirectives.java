package arch.sic;

import java.util.ArrayList;
import asm.ConHex;

public class AsmDirectives {

	private ArrayList<AsmTabRecord> asmDirectives = new ArrayList<AsmTabRecord>();

	public AsmDirectives() {
		asmDirectives.add(new AsmTabRecord("START", 0));
		asmDirectives.add(new AsmTabRecord("END", 0));
		asmDirectives.add(new AsmTabRecord("RESW", 3));
		asmDirectives.add(new AsmTabRecord("RESB", 1));
		asmDirectives.add(new AsmTabRecord("WORD", 3));
		asmDirectives.add(new AsmTabRecord("BYTE", 1));;
	}
	
	// wrapper for compatibility reasons
	public int getSize(String opcode, String operand) {
		Integer size = 0;
		try {
			size = getSize(opcode, Integer.parseInt(operand));
		} catch ( NumberFormatException nfe ) {
			operand = operand.toUpperCase();
			
			if ( operand.startsWith("=C'")  && operand.endsWith("'")) {
				size = operand.length()-4;
			}
			
			if ( operand.startsWith("=X'") && operand.endsWith("'")) {
				operand = operand.substring(3,operand.length()-1);
				operand = ConHex.hexToInt(operand);
				size = getSize(opcode, operand);
			}
		}
		return size;
	}
	
	// get size of [Data-type x Count]
	public int getSize(String opcode, Integer arg) {
		for ( AsmTabRecord tuple : asmDirectives ) {
			if ( tuple.label.equalsIgnoreCase(opcode) == true ) {
				// match found. compute next offset from integer arg
				if ( opcode.equals("WORD") || opcode.equals("BYTE") )
					return tuple.count;
				return tuple.count*arg;
			}
		}
		return -1;
	}
	
	// Pads zeros to data smaller than -size-
	String autoPad(String data, Integer size) {
		while ( data.length() < size)
			data = "0" + data;
		return data;
	}
	
	// parse data-type and data to retrieve bytes
	public String getData(String opcode, String operand) {
		
		String dataOut = null;

		if ( operand.startsWith("=C'")  && operand.endsWith("'")) {
			operand = operand.substring(3,operand.length()-1);
		}
		
		if ( operand.startsWith("=X'") && operand.endsWith("'")) {
			operand = operand.substring(3,operand.length()-1);
			operand = ConHex.hexToInt(operand);
		}
		
		// RESB = Pad '00' x N times
		if ( opcode.equalsIgnoreCase("RESB") ) {
    		int count = Integer.parseInt(operand);
    		dataOut = "";
    		for ( int c = 0; c < count; c++)
    			dataOut = dataOut + "00";
    	}
		
		// RESW = Pad '000000' x N times
    	if ( opcode.equalsIgnoreCase("RESW")) {
    		int count = Integer.parseInt(operand);
    		dataOut = "";
    		for ( int c = 0; c < count; c++)
    			dataOut = dataOut + "000000";
    	}
    	
    	// BYTE = Parse operand to Int=>Hex and make it 1 byte
    	if ( opcode.equalsIgnoreCase("BYTE")) {
    		
    		try {
    			operand = ConHex.intToHex(Integer.parseInt(operand));
    			dataOut = autoPad(operand, 2);
    		} catch ( NumberFormatException nfe ) {
    			String temp;
    			dataOut = "";
    			for (int i = 0; i < operand.length(); i++) {
    				temp = ConHex.intToHex((int) operand.toCharArray()[i]);
    				dataOut = temp + dataOut;
    			}
    		}
    	}
    	
    	// WORD = Parse operand to Int=>Hex and make it 3 bytes
    	if ( opcode.equalsIgnoreCase("WORD")) {
    		operand = ConHex.intToHex(Integer.parseInt(operand));
    		dataOut = autoPad(operand,6);
    	}
    
		return dataOut;
	}
	
}
